
from django.db import models
from accounts.models import User
from django import forms

OPERATOR_CHOICES = (
    ('', '-----------'),
    ('AND', 'AND'),
    ('OR','OR'),
)

class Vendor(models.Model):
    name = models.CharField('Name', max_length=100)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Vendor'
        verbose_name_plural = 'Vendors'
        ordering = ['name']

    def __str__(self):
        return self.name

class Usecase(models.Model):

    logsource = models.ForeignKey('logsource.Logsource', verbose_name='Logsource')
    high = models.CharField('High_level', max_length=100, blank=False)
    number = models.BigIntegerField('UC_#', blank=False)
    type = models.CharField('UC_type', max_length=100, blank=False)
    description = models.TextField('UC description',blank=False, null=True)
    engineer = models.ForeignKey(User, blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Usecase'
        verbose_name_plural = 'Usecases'
        ordering = ['number']

    def __str__(self):
        return str(self.number)

class Category(models.Model):

    name = models.CharField('Name', max_length=100, blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ['name']

    def __str__(self):
        return self.name

class Test(models.Model):
    id = models.BigIntegerField('RTID', unique=True,primary_key=True,blank=False)
    rtparameter = models.CharField('RT_parameters', max_length=100, blank=False)
    description = models.TextField('RT_description', blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Test'
        verbose_name_plural = 'Tests'
        ordering = ['parameter']

    def __str__(self):
        return str(self.id)


class Rule(models.Model):
    logsource = models.ForeignKey('logsource.Logsource', verbose_name='Logsource')
    name = models.CharField('Rule name', max_length=100, blank=False)
    parameters = models.IntegerField('N_parameters', blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Rule'
        verbose_name_plural = 'Rules'
        ordering = ['name']

    def __str__(self):
        return self.name


class Qradaridentifier(models.Model):
    id = models.BigIntegerField('QID', unique=True,primary_key=True,blank=False)
    logsource = models.ForeignKey('logsource.Logsource', verbose_name='Logsource')
    name = models.CharField('Qname', max_length=100, blank=False)
    high = models.CharField('High_level', max_length=100, blank=False)
    low = models.CharField('Low_level', max_length=100, blank=False)
    description = models.TextField('Q_description', blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Qradaridentifier'
        verbose_name_plural = 'Qradaridentifiers'
        ordering = ['name']

    def __str__(self):
        return str(self.id)

class Parameter(models.Model):

    rid = models.ForeignKey('logsource.Rule', verbose_name='RID')
    r_parameter = models.IntegerField('Rule Parameter', blank=False)
    operator = models.CharField('Logic operator', max_length=100, blank=False)
    parameters = models.TextField('Parameter', blank=False)
    rtid = models.ForeignKey('logsource.Test', verbose_name='RTID')
    validation = models.BooleanField(default=True)
    valid_option = models.CharField('Valid Option', max_length=100, blank=True)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Parameter'
        verbose_name_plural = 'Parameters'
        ordering = ['id']

    def __str__(self):
        return str(self.rtid)

class Logsource(models.Model):
    id = models.IntegerField('LSID', unique=True,primary_key=True,blank=False)
    name = models.CharField('LS_Name', max_length=100,blank=False)
    vendor = models.ForeignKey('logsource.Vendor',blank=False, verbose_name='Vendor')
    type = models.CharField('LS_type', max_length=100,blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Logsource'
        verbose_name_plural = 'Logsources'
        ordering = ['name']

    def __str__(self):
        return self.name


class Cep(models.Model):
    qid = models.ForeignKey('logsource.Qradaridentifier', verbose_name='QID')
    name = models.CharField('Name', max_length=100, blank=False)
    cepnumber = models.IntegerField('Number', blank=False)
    regex = models.TextField('Regex', blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Cep'
        verbose_name_plural = 'Ceps'
        ordering = ['name']

    def __str__(self):
        return self.name

class Detail(models.Model):
    category = models.ForeignKey('logsource.Category',blank=False, verbose_name='Category')
    ucnum = models.BigIntegerField(blank=False,verbose_name='Use Case Number')
    ucdescription = models.TextField(blank=False, verbose_name='Description')
    logsourcetype = models.TextField(blank=False,verbose_name='Log Type')
    rulename = models.ForeignKey('logsource.Rule', verbose_name='Rule Name')
    operator_one = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    parameter_one = models.TextField('Parameter' ,blank=True)
    test_description_one = models.TextField('RT Description',blank=True)
    regex_one = models.TextField('Regex',blank=True)
    operator_two = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    parameter_two = models.TextField('Parameter',blank=True)
    test_description_two = models.TextField('RT Description',blank=True)
    regex_two = models.TextField('Regex',blank=True)
    operator_three = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    parameter_three = models.TextField('Parameter',blank=True)
    test_description_three = models.TextField('RT Description',blank=True)
    regex_three = models.TextField('Regex',blank=True)
    operator_four = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    parameter_four = models.TextField('Parameter',blank=True)
    test_description_four = models.TextField('RT Description',blank=True)
    regex_four = models.TextField('Regex',blank=True)
    operator_five = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    test_description_five = models.TextField('RT Description',blank=True)
    parameter_five = models.TextField('Parameter',blank=True)
    regex_five = models.TextField('Regex',blank=True)
    operator_six = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    parameter_six = models.TextField('Parameter',blank=True)
    test_description_six = models.TextField('RT Description',blank=True)
    regex_six = models.TextField('Regex',blank=True)
    operator_seven = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    parameter_seven = models.TextField('Parameter',blank=True)
    test_description_seven = models.TextField('RT Description',blank=True)
    regex_seven = models.TextField('Regex',blank=True)
    operator_eight = models.CharField(max_length=3, choices=OPERATOR_CHOICES, blank=True)
    parameter_eight = models.TextField('Parameter',blank=True)
    test_description_eight = models.TextField('RT Description',blank=True)
    regex_eight = models.TextField('Regex',blank=True)
    engineer = models.ForeignKey(User, blank=False)
    created = models.DateTimeField('Created', auto_now_add=True)
    modified = models.DateTimeField('Modified', auto_now=True)

    class Meta:
        verbose_name = 'Detail'
        verbose_name_plural = 'Details'
        ordering = ['id']

    def __str__(self):
        return str(self.id)
