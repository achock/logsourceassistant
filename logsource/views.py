# coding=utf-8

from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.views.generic import (
    CreateView, TemplateView, UpdateView, FormView,ListView
)
from django.db import connection
from .models import Logsource,Vendor,Test,Usecase,Category,Rule,Parameter,Qradaridentifier,Cep,Detail


class IndexView(TemplateView):

    template_name = 'logsource/logs.html'



class LogsourceListView(ListView):

    model = Logsource
    template_name = 'logsource/logsource.html'
    context_object_name = 'logs'
    success_url = reverse_lazy('index')

logsource_list = LogsourceListView.as_view()


def logsource_list(request):
    #cursor = connection.cursor()
    #cursor.execute("Select uc.high as category,uc.description as description,ls.type as ltype ,pr.validation as validation, cep.regex as cregex FROM logsource_usecase as uc, logsource_logsource as ls,logsource_parameter as pr, logsource_test as ts,logsource_cep as cep, logsource_qradaridentifier as qid WHERE ls.id = uc.logsource_id AND ls.id = qid.logsource_id AND qid.id = cep.qid_id")
    #cursor.execute("Select uc.high as category,uc.description as description,ls.type as ltype ,pr.validation as validation, cep.regex as cregex FROM logsource_usecase as uc, logsource_logsource as ls,logsource_parameter as pr, logsource_test as ts,logsource_cep as cep, logsource_qradaridentifier as qid")
    context = {
        'log_list': Detail.objects.all()
    }

    return render(request, 'logsource/logsource_list.html',context)


index = IndexView.as_view()
