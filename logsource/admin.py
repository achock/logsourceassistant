# coding=utf-8

from django.contrib import admin
from .models import Logsource,Vendor,Test,Usecase,Category,Rule,Parameter,Qradaridentifier,Cep,Detail

class LogsourceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'vendor')
    search_fields = ('name', 'type')
    list_filter = ('created',)

class VendorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('id','name')
    list_filter = ('name',)

class UsecaseAdmin(admin.ModelAdmin):
    list_display = ('id', 'logsource','high','number','type','description','engineer')
    search_fields = ('id','type','number')
    list_filter = ('logsource','high','number')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('id','name')
    list_filter = ('name',)

class TestAdmin(admin.ModelAdmin):
    list_display = ('id', 'rtparameter','description')
    search_fields = ('id','rtparameter','description')
    list_filter = ('rtparameter',)

class RuleAdmin(admin.ModelAdmin):
    list_display = ('id', 'logsource','name','parameters')
    search_fields = ('id','logsource','name','parameters')
    list_filter = ('parameters',)

    def get_form(self, request, obj=None, **kwargs):
        form = super(RuleAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['name'].queryset = Rule.objects.all().order_by('name')

        return form

class QidAdmin(admin.ModelAdmin):
    list_display = ('id', 'logsource','name','high','low','description')
    search_fields = ('id', 'logsource','name','high','low','description')
    list_filter = ('name',)

class ParameterAdmin(admin.ModelAdmin):
    list_display = ('id', 'rid','r_parameter','operator','parameters','rtid','validation','valid_option')
    search_fields = ('id', 'logsource','name','high','low','description')
    list_filter = ('rtid',)

    def get_form(self, request, obj=None, **kwargs):
        form = super(ParameterAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['rtid'].queryset = Test.objects.all().order_by('id')

        return form

class CepAdmin(admin.ModelAdmin):
    list_display = ('id', 'name','qid','cepnumber','regex')
    search_fields = ('id', 'name','qid','cepnumber','regex')
    list_filter = ('qid',)

class DetailAdmin(admin.ModelAdmin):
    list_display = ('category', 'ucnum','id','logsourcetype')
    search_fields = ('category', 'ucnum','id','logsourcetype')
    list_filter = ('category',)

    def queryset(self, request):
        qs = super(DetailAdmin, self).queryset(request)
        return Usecase.objects.all().order_by('number')

    def get_form(self, request, obj=None, **kwargs):
        form = super(DetailAdmin, self).get_form(request, obj, **kwargs)
        #form.base_fields['ucnum'].queryset = Usecase.objects.all().order_by('number')
        #form.base_fields['ucnum'].queryset = Usecase.objects.filter(number__iexact='number')
        #form.base_fields['ucnum'].queryset = Usecase.objects.all().order_by('number')
        # form.base_fields['logsourcetype'].queryset = Logsource.objects.all().order_by('type')
        # form.base_fields['ucdescription'].queryset = Logsource.objects.all().order_by('description')
        # form.base_fields['parameter_one'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['parameter_two'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['parameter_three'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['parameter_four'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['parameter_five'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['parameter_six'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['parameter_seven'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['parameter_eight'].queryset = Logsource.objects.all().order_by('parameters')
        # form.base_fields['test_description_one'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['test_description_two'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['test_description_three'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['test_description_four'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['test_description_five'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['test_description_six'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['test_description_seven'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['test_description_eight'].queryset = Test.objects.all().order_by('description')
        # form.base_fields['regex_one'].queryset = Cep.objects.all().order_by('regex')
        # form.base_fields['regex_two'].queryset = Cep.objects.all().order_by('regex')
        # form.base_fields['regex_three'].queryset = Cep.objects.all().order_by('regex')
        # form.base_fields['regex_four'].queryset = Cep.objects.all().order_by('regex')
        # form.base_fields['regex_five'].queryset = Cep.objects.all().order_by('regex')
        # form.base_fields['regex_six'].queryset = Cep.objects.all().order_by('regex')
        # form.base_fields['regex_seven'].queryset = Cep.objects.all().order_by('regex')
        # form.base_fields['regex_eight'].queryset = Cep.objects.all().order_by('regex')

        return form

admin.site.register(Logsource,LogsourceAdmin)
admin.site.register(Vendor,VendorAdmin)
admin.site.register(Test,TestAdmin)
admin.site.register(Usecase,UsecaseAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(Rule,RuleAdmin)
admin.site.register(Parameter,ParameterAdmin)
admin.site.register(Qradaridentifier,QidAdmin)
admin.site.register(Cep,CepAdmin)
admin.site.register(Detail,DetailAdmin)
