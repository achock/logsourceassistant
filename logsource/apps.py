from django.apps import AppConfig


class LogsourceConfig(AppConfig):
    name = 'logsource'
    verbose_name = 'RCA'
