# coding=utf-8

from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return render(request, 'index.html')


def logsource_list(request):
    return render(request, 'logsource_list.html')


def logsource(request):
    return render(request, 'logsource.html')
