"""logsourceassis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.static import serve as serve_static
from django.contrib.auth.views import login, logout

from core import views

admin.site.site_title = 'RCA Admin'
admin.site.site_header = 'RCA'
admin.site.index_title = 'Welcome to RCA'

urlpatterns = [
    url(r'^$', login, {'template_name': 'login.html'}, name='login'),
    url(r'^index/$', views.index, name='index'),
    url(r'^logsources/', include('logsource.urls', namespace='logsource')),
    url(r'^account/', include('accounts.urls', namespace='accounts')),
    url(r'^leave/$', logout, {'next_page': 'index'}, name='logout'),
    url(r'^admin/', admin.site.urls),
]
