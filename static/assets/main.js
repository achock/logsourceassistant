$(document).ready(function() {
  let table = $('#logsource').DataTable();

   $('#logsource').on('click', 'tr', function () {
        let data = table.row( this ).data();
        let detail_one = data[5] + ' ' + data[21] + ' ' +   data[13] + '\n';
        let detail_two =  data[6] + ' ' +  data[22] + ' ' +   data[14] + '\n';
        let detail_three = data[7] + ' ' +  data[23] + ' ' +  data[15] + '\n';
        let detail_four = data[8] + ' ' +  data[24] + ' ' +   data[16] + '\n';
        let detail_five = data[9] + ' ' +  data[25] + ' ' +   data[17] + '\n';
        let detail_six = data[10] + ' ' +  data[26] + ' ' +   data[18] + '\n';
        let detail_seven = data[11] + ' ' +  data[27] + ' ' +  data[19] + '\n';
        let detail_eight = data[12] + ' ' +  data[28] + ' ' +   data[20] + '\n';
        let detail_info = detail_one + detail_two + detail_three + detail_four + detail_five + detail_six + detail_seven + detail_eight;
        $('#details').append(detail_info);
        $("#regex_one").val(data[29]);
        $("#regex_two").val(data[30]);
        $("#regex_three").val(data[31]);
        $("#regex_four").val(data[32]);
        $("#regex_five").val(data[33]);
        $("#regex_six").val(data[34]);
        $("#regex_seven").val(data[35]);
        $("#regex_eight").val(data[36]);
        $("#engineer").val(data[37]);
        $('#DescModal').modal("show");
        $('input').each(function() {
          if ($(this).val() === '') {
            $(this).parent().css( "display", "none" );
          }
        });
    } );

    $("#btnclose").on("click", function() {
     $('#DescModal').modal("hide");
     $('#details').html('');
     $('input').each(function() {
       if ($(this).val() != '') {
         $(this).val('');
       }
     });
   });

   $(".close").on("click", function() {
    $('#DescModal').modal("hide");
    $('#details').html('');
    $('input').each(function() {
      if ($(this).val() != '') {
        $(this).val('');
      }
    });
  });

  $('#DescModal').on('hidden.bs.modal', function () {
    $('#details').html('');
    $('input').each(function() {
      if ($(this).val() != '') {
        $(this).val('');
      }
    });
  })

   //$('#logsource tr td:gt(3)').hide();
   //$('#logsource tr th:gt(3)').hide();
} );
